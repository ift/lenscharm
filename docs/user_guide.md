# Introduction to LensCharm


## 1. Examples

- [Basic example and overview](examples/basic_example.ipynb)
- [Advanced example: Setting the model parameters](examples/advanced_example.ipynb)


## 2. Configuration

The basic usage of LensCharm is to use the lens_charm.py script.

### Basic example
To run the basic example please run:
```python
python lens_charm.py config/config_basic.yaml
```

### Advanced example
To run the advanced example please run:
```python
python lens_charm.py config/config_advanced.yaml
```


# Overview of the steps to model with LensCharm
1. Define the model
2. Load the data
3. Adjust the prior parameters such that the model roughly resembles the data.
4. Reconstruct
