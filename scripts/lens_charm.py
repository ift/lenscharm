from charm_lensing import (
    plotting, build_lens_system, data_loader,
    config_parser, minimization_parser,
    nifty_connect)
import numpy as np
import nifty8 as ift

from os.path import join
# from os import makedirs
# import argparse

from sys import exit

import jax
jax.config.update("jax_platform_name", "cpu")


if __name__ == '__main__':
    cfg = config_parser.load_and_save_config(
        default='../configs/basic_example.yaml')

    lens_system = build_lens_system.build_lens_system(cfg)

    # data, response, and likelihood
    if cfg.get('mock', False):
        likelihood, data = data_loader.MockDataLoadingStrategy(
            cfg,
            'sky',
            lens_system.lens_plane_model.space,
            (lens_system.lens_plane_model, lens_system.source_plane_model)
        )
    else:
        likelihood, data = data_loader.RealDataLoadingStrategy(
            cfg,
            'sky',
            lens_system.lens_plane_model.space,
        )

    # Connect likelihood to model
    likelihood_full = nifty_connect.connect_likelihood_to_model(
        likelihood, lens_system.get_forward_model_full())
    likelihood_para = nifty_connect.connect_likelihood_to_model(
        likelihood, lens_system.get_forward_model_parametric())

    # Plot prior samples
    if cfg['plotting_options']['prior_samples']:
        np.random.seed(cfg['seed'])
        for ii in range(cfg['plotting_options'].get('prior_samples', 3)):
            sample = ift.from_random(likelihood_full.domain)

            if (cfg.get('mock', False) or cfg['plotting_options'].get('mock_data', False)):
                plotting.mock_data_plot(
                    position=sample.val,
                    output_name_template=None,
                    data=data,
                    lens_system=lens_system,
                    iteration=0,
                    parametric=False,
                    log_data=cfg['plotting_options']['log_data']
                )

            else:
                plotting.standard_data_plot(
                    position=sample.val,
                    output_name_template=None,
                    data=data,
                    lens_system=lens_system,
                    iteration=0,
                    parametric=False,
                    log_data=cfg['plotting_options']['log_data']
                )

    print('Output directory:', cfg['outputdir'])

    # MINIMIZATION
    # Minimization Parametric
    n_samps_par = minimization_parser.n_samples_factory(
        cfg['minimization']['parametric'])
    lin_samp_par = minimization_parser.linear_sampling_factory(
        cfg['minimization']['parametric'])
    non_samp_par = minimization_parser.nonlinear_sampling_factory(
        cfg['minimization']['parametric'])
    minimizer_par = minimization_parser.minimizer_factory(
        cfg['minimization']['parametric'])

    # Initialize seed for the reconstruction
    np.random.seed(cfg['seed'])
    ift.random.push_sseq_from_seed(cfg['seed'])

    # parametric minimization
    samples = ift.optimize_kl(
        likelihood_para,
        cfg['minimization']['parametric']['n_total_iterations'],
        n_samps_par,
        minimizer_par,
        lin_samp_par,
        non_samp_par,
        output_directory=join(cfg['outputdir'], 'save_parametric'),
        plot_minisanity_history=False,
        initial_position=None,
        resume=cfg['minimization']['parametric']['resume'],
        inspect_callback=plotting.nifty_plotting_wrapper(
            plotting.plot_check_factory(
                outputdir=cfg['outputdir'],
                data=data,
                cfg=cfg,
                lens_system=lens_system,
                parametric=True,
                min_iterations=0,
                operators_samples=plotting.samples_dictionary_parser(
                    cfg['plotting_options'].get('samples', False),
                    lens_system))),
    )

    # Initialize seed for the reconstruction
    np.random.seed(cfg['seed'])
    ift.random.push_sseq_from_seed(cfg['seed'])

    n_samps_full = minimization_parser.n_samples_factory(
        cfg['minimization']['full'])
    lin_samp_full = minimization_parser.linear_sampling_factory(
        cfg['minimization']['full'])
    non_samp_full = minimization_parser.nonlinear_sampling_factory(
        cfg['minimization']['full'])
    minimizer_full = minimization_parser.minimizer_factory(
        cfg['minimization']['full'])
    initial_position = minimization_parser.domain_transition(
        likelihood_full.domain, samples)

    # full model minimization
    samples = ift.optimize_kl(
        likelihood_full,
        cfg['minimization']['full']['n_total_iterations'],
        n_samps_full,
        minimizer_full,
        lin_samp_full,
        non_samp_full,
        output_directory=join(cfg['outputdir'], 'save_full'),
        initial_position=initial_position,
        resume=cfg['minimization']['full']['resume'],
        inspect_callback=plotting.nifty_plotting_wrapper(
            plotting.plot_check_factory(
                outputdir=cfg['outputdir'],
                data=data,
                cfg=cfg,
                lens_system=lens_system,
                parametric=False,
                min_iterations=cfg['minimization']['parametric']['n_total_iterations'],
                operators_samples=plotting.samples_dictionary_parser(
                    cfg['plotting_options'].get('samples', False),
                    lens_system))),
    )

    if cfg['calculate_elbo']:
        elbo_stats = ift.estimate_evidence_lower_bound(
            ift.StandardHamiltonian(likelihood_full),
            samples,
            200)
