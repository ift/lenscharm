from charm_lensing import (build_lens_system, data_loader, utils)
from charm_lensing.likelihood import connect_likelihood_to_model
import yaml
from numpy import allclose

import jax
jax.config.update('jax_platform_name', 'cpu')


def test_full_stack():
    cfg_file = './tests/configs/fullstack_config.yaml'
    with open(cfg_file, 'r') as file:
        cfg = yaml.safe_load(file)

    lens_system = build_lens_system.build_lens_system(cfg)

    # data, response, and likelihood
    likelihood, data = data_loader.RealDataLoadingStrategy(
        cfg,
        'sky',
        lens_system.lens_plane_model.space,
        nifty_connect=False)

    likelihood_full = connect_likelihood_to_model(
        likelihood,
        lens_system.get_forward_model_full()
    )
    sample = utils.from_random(likelihood_full.domain, seed=0)
    assert allclose(likelihood_full(sample), 119291.05236556346)
