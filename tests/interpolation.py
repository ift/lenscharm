import jax
from charm_lensing.spaces import get_xycoords
import nifty8 as ift
import numpy as np

from charm_lensing.spaces import Space
from charm_lensing.interpolation import build_linear_interpolation
from typing import Tuple


class Contractions(ift.LinearOperator):
    def __init__(self, domain, target, jj, quadrants):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(target)
        self._jj = jj
        self._domain_length = np.multiply(*self._domain.shape)
        self._quadrants = quadrants
        self._capability = self.TIMES | self.ADJOINT_TIMES

    @ staticmethod
    def forward(f, jj, quadrants):
        return np.sum(f[jj]*quadrants[0]*quadrants[1], axis=0)

    @ staticmethod
    def backward(f, jj, quadrants, domain_length):
        return np.bincount(
            jj.reshape(-1),
            (quadrants[0]*quadrants[1]*f).reshape(-1),
            minlength=domain_length
        )

    def apply(self, x, mode):
        self._check_input(x, mode)
        x_val = x.val.reshape(-1)

        if mode == self.TIMES:
            res = self.forward(x_val, self._jj, self._quadrants)
        else:
            res = self.backward(
                x_val,
                self._jj,
                self._quadrants,
                self._domain_length).reshape(self._domain.shape)
        return ift.Field(self._tgt(mode), res)


class PartialVdotOperator(ift.LinearOperator):
    def __init__(self, mat):
        self._domain = mat.domain
        shp = mat.domain.shape
        self._target = ift.makeDomain(ift.UnstructuredDomain(shp[1]))
        self._mat = mat.val
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        v = x.val
        if mode == self.TIMES:
            val = np.sum(self._mat*v, axis=0)
            return ift.Field.from_raw(self._target, val)
        else:
            val = self._mat*v
            return ift.Field.from_raw(self._domain, val)


class LinearInterpolation(ift.Operator):
    """
    Multilinear interpolation for variable points in an RGSpace

    Parameters
    ----------
    rgrid_dom: Space
        regular grid, 2D space, the domain of the field to be interpolated.

    rgrid_key: str
        key of the interpolated field domain (in multifield dictionary).

    points_shape: Tuple
        shape of the interpolation points.

    points_key: str
        key of the points domain (in multifield dictionary).
    """

    def __init__(
        self,
            rgrid_dom: Space,
            rgrid_key: str,
            points_shape: Tuple[int, int],
            points_key: str
    ) -> None:
        # Checks and balances
        if not isinstance(rgrid_dom, Space):
            raise TypeError
        if len(rgrid_dom.shape) != 2:
            raise TypeError
        if len(points_shape) != 2:
            raise ValueError('Point domain shape length incompatible')
        if points_shape[0] != len(rgrid_dom.shape):
            raise ValueError('Point domain incompatible with RGSpace')

        self._domain = ift.MultiDomain.make({
            rgrid_key: rgrid_dom.nifty_domain,
            points_key: ift.UnstructuredDomain(points_shape)
        })
        self._target = ift.makeDomain(ift.UnstructuredDomain(points_shape[1]))
        self._rgrid_key = rgrid_key
        self._point_key = points_key

        # TODO, FIXME: Does this do a half-pixel shift, a full one, or none?
        upper_left = rgrid_dom.coords()[..., 0, 0]
        self.upper_left = upper_left[..., None]

        self._rgrid_dom = rgrid_dom.nifty_domain
        self._N_dim, self._N_points = points_shape
        self._A = Contractions(self.domain[self._rgrid_key][0],
                               self.target[0],
                               None,
                               None)

    def build_xy(self, sampling_points):
        """This method computes indices jj and weights for interpolation.
        It takes the sampling points as input,
        calculates the positions in the RGSpace,
        and returns the indices and weights."""

        sampling_points = sampling_points - self.upper_left
        mg = np.mgrid[(slice(0, 2),)*self._N_dim]
        mg = np.array(list(map(np.ravel, mg)))
        # dist = [list(dom.distances) for dom in self._rgrid_dom]
        dist = self._rgrid_dom.distances
        # FIXME This breaks as soon as not all domains have the same number of
        # dimensions.
        dist = np.array(dist).reshape(-1, 1)
        pos = sampling_points/dist
        excess = pos - np.floor(pos)
        pos = np.floor(pos).astype(np.int64)
        max_index = np.array(self._rgrid_dom.shape).reshape(-1, 1)

        # NOTE: Casting all points outside of domain to zero
        outside = np.any((pos > max_index) + (pos < 0), axis=0)

        weights = np.zeros((2, len(mg[0]), self._N_points))
        jj = np.zeros((len(mg[0]), self._N_points), dtype=np.int64)

        for i in range(len(mg[0])):
            quadrant = np.abs(1 - mg[:, i].reshape(-1, 1) - excess)
            weights[:, i, :] = quadrant
            weights[:, i, outside] = 0.
            fromi = (pos + mg[:, i].reshape(-1, 1)) % max_index
            jj[i, :] = np.ravel_multi_index(fromi, self._rgrid_dom.shape)

        return jj, weights

    def apply(self, x):
        self._check_input(x)
        lin = isinstance(x, ift.Linearization)
        xval = x
        if lin:
            xval = x.val

        jj, quadrants = self.build_xy(xval[self._point_key].val)

        self._A._jj = jj
        self._A._quadrants = quadrants

        if not lin:
            return self._A(x[self._rgrid_key])

        grad = ift.Field.from_raw(
            x.target[self._point_key],
            self.grad(jj, quadrants, xval[self._rgrid_key].val.reshape(-1))
            / np.array(self.domain[self._rgrid_key][0].distances)[..., None]
        )

        # FIXME: this can be done better
        val = self._A(xval[self._rgrid_key])
        d1 = self._A.ducktape(self._rgrid_key)
        d2 = PartialVdotOperator(grad).ducktape(self._point_key)
        return x.new(val, (d1+d2)(x.jac))

    def grad(self, jj, quadrants, f):
        ff = f[jj]
        gradx = np.sum(
            ff*quadrants[1]*np.array((-1, -1, 1, 1))[..., None], axis=0)
        grady = np.sum(
            ff*quadrants[0]*np.array((-1, 1, -1, 1))[..., None], axis=0)
        return np.array((gradx, grady))


def test_interpolation():
    source_space = Space((128,)*2, 0.1)
    field = jax.random.normal(jax.random.PRNGKey(0), source_space.shape)

    dom_shape = (256,)*2
    dom_dist = 0.1
    points = (get_xycoords(dom_shape, dom_dist) + 5.3).reshape(2, -1)
    points += jax.random.normal(jax.random.PRNGKey(0), points.shape)

    o_model = LinearInterpolation(
        source_space, 'source',
        points.shape, 'lens')

    n_model = build_linear_interpolation(
        ('source', source_space),
        ('lens', points.shape)
    )

    out_o = o_model(
        ift.makeField(o_model.domain, {'lens': points, 'source': field}))
    out_n = n_model({'lens': points, 'source': field})

    assert np.allclose(out_o.val, out_n)
